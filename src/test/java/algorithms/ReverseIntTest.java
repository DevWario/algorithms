package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.function.IntFunction;
import java.util.stream.Stream;

class ReverseInt {
	
	IntFunction<Stream<String>> convertToStream = number -> {
		int absolute = Math.abs(number);
		String numberString = Integer.toString(absolute);
		String[] arrayString = numberString.split("");
		return Arrays.stream(arrayString);
	}; 
	
	public int reverse(int number) {
		Stream<String> stream = convertToStream.apply(number);
		String reversed = stream.reduce("", (parcialString, next) -> next + parcialString);
		return  Math.round(Integer.parseInt(reversed) * Math.signum(number));
	}

}


public class ReverseIntTest {
	
	ReverseInt reverser = new ReverseInt();
	
	@Test 
	public void test15() {
		int origin = 15;
		int actual = reverser.reverse(origin);
		assertEquals(51, actual);
		
	}
	
	@Test
	public void test189() {
		int origin = 189;
		int actual = reverser.reverse(origin);
		assertEquals(981, actual);
		
	}
	
	@Test 
	public void test500() {
		int origin = 500;
		int actual = reverser.reverse(origin);
		assertEquals(5, actual);
		
	}
	
	@Test 
	public void testMinus15() {
		int origin = -15;
		int actual = reverser.reverse(origin);
		assertEquals(-51, actual);
		
	}
	
	@Test 
	public void testMinus90() {
		int origin = -90;
		int actual = reverser.reverse(origin);
		assertEquals(-9, actual);
		
	}

}

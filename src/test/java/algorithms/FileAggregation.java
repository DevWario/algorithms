package algorithms;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

public class FileAggregation {


    @Test
    public void testFileAggregation(){
        try {
            Files.walk(Path.of("./src/../src/main/resources"))
                    .map(it -> it.normalize())
                    .collect(Collectors.toSet())
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

package algorithms;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class AlternateBeginEndAlgarism {

    @Test
    public void given1234Than1423(){
        int numberOrigin = 1234;
        int expect = 1423;

        Integer[] numbers = new Integer[10];

        int number = numberOrigin;
        int index = 0;
        while (number > 0){
            int rest = number % 10;
            System.out.print("Number: "+ rest);
            numbers[index] = rest;
            number = number / 10;
            index++;
        }

        //Instead of 1234 now we have 4321
        //We can invert the itens to get the same result: ex: get last get first, move the pointer to next and previous
        Integer[] numbersChanged = new Integer[index];
        int first = 0;
        int last = numbersChanged.length - 1;
        int indx = 0;
        while(first <= last){
            if(first != last){
                numbersChanged[indx++] = numbers[last];
                numbersChanged[indx++] = numbers[first];
            } else {
                numbersChanged[indx++] = numbers[last];
            }
            last--;
            first++;
        }
        System.out.println(Arrays.toString(numbersChanged));

    }
}

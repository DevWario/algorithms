package algorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class SecondLargeElementTest {
	
	SecondLargeElement secondLarge = new SecondLargeElement();

	@Test
	public void testSimpleArray() {
		int[] array = {1,3,5,7,9,13,15};
		int actual = secondLarge.find(array);
		assertEquals(13, actual);
	}

	@Test
	public void testInvertedArray() {
		int[] array = {15,13,9,7,5,3,1};
		int actual = secondLarge.find(array);
		assertEquals(13, actual);
	}

	@Test
	public void testRandomArray() {
		int[] array = {15,133,9,75,5,38,1};
		int actual = secondLarge.find(array);
		assertEquals(75, actual);
	}

}

package algorithms;

import java.util.Scanner;

import algorithms.structures.UnionFind;

public class DynamicConnectivity {
	
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

    	int size = Integer.parseInt(scanner.nextLine());
    	UnionFind uf = new UnionFind(size);

		String[] elementsString = scanner.nextLine().split(" ");
    	while(!elementsString[0].equals("") && elementsString.length == 2) {
    		
            int element1 = Integer.parseInt(elementsString[0]);
            int element2 = Integer.parseInt(elementsString[1]);
            if(!uf.connected(element1, element2)) {
            	uf.union(element1, element2);
            	System.out.println("New elements indexed: "+element1+" "+element2);
            }
    		elementsString = scanner.nextLine().split(" ");
    		
    	}
        
        
	}
    

	
	
	

}

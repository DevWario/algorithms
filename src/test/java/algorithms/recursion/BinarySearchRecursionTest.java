package algorithms.recursion;

import static org.junit.jupiter.api.Assertions.assertEquals;

import algorithms.BinarySearchRecursion;
import org.junit.jupiter.api.Test;

public class BinarySearchRecursionTest {

	@Test
	public void testarMenor() {
		BinarySearchRecursion search = new BinarySearchRecursion(new int[] {1,3, 5, 7, 9});
		int index = search.search(3);
		assertEquals(index, 1);
	}

	@Test
	public void testarMaior() {
		BinarySearchRecursion search = new BinarySearchRecursion(new int[] {1,3, 5, 7, 9});
		int index = search.search(9);
		assertEquals(index, 4);
	}


	@Test
	public void testarIgual() {
		BinarySearchRecursion search = new BinarySearchRecursion(new int[] {1,3, 5, 7, 9});
		int index = search.search(5);
		assertEquals(index, 2);
	}

	@Test
	public void testarNaoTem() {
		BinarySearchRecursion search = new BinarySearchRecursion(new int[] {1,3, 5, 7, 9});
		int index = search.search(8);
		assertEquals(index, -1);
	}

}

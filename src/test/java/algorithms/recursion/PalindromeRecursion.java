package algorithms.recursion;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PalindromeRecursion {
	
	private boolean isPalindrome(String word, int indexBegin, int indexEnd) {
		if(indexBegin >= indexEnd) {
			return true;
		}
		return word.charAt(indexBegin) == word.charAt(indexEnd) 
				&& isPalindrome(word, indexBegin + 1, indexEnd - 1);
	}
	
	@Test
	public void testPalindromeTrue() {
		
		String word = "paelleap";
		boolean isPalindrome = isPalindrome(word, 0, word.length()-1);
		assertTrue(isPalindrome);
		
	}

	@Test
	public void testPalindromeFalse() {
		String word = "paellea";
		boolean isPalindrome = isPalindrome(word, 0, word.length()-1);
		assertFalse(isPalindrome);
	}

}

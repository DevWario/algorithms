package algorithms.recursion;

import static org.junit.jupiter.api.Assertions.assertEquals;

import algorithms.MaximumRecursive;
import org.junit.jupiter.api.Test;

public class MaximumRecursiveTest {
	
	MaximumRecursive maximumTest = new MaximumRecursive();
	
	@Test
	public void testMaximum() {
		int[] array = {6, 3, 7, 2, 9, 1, 0, 7 ,6};
		int result = maximumTest.maximum(array, array.length - 1);
		int expected = 9;
		assertEquals(expected, result);
	}

}

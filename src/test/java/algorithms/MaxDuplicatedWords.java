package algorithms;

import java.util.HashMap;
import java.util.Map;

public class MaxDuplicatedWords {
	
	public int getMaxDuplicatedWords(String string) {
		
		String[] words = string.split(" ");
		Map<String, Integer> map = new HashMap<>();
		int max = Integer.MIN_VALUE;
		
		for (int i = 0; i < words.length; i++) {
			Integer count = map.get(words[i]);
			count = count == null ? 0 : count + 1;
			map.put(words[i], count);
		}

		/*
		LinkedHashMap<String, Integer> mapOrdered;
		mapOrdered = map.entrySet().stream()
		.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
		.collect(Collectors.toMap(
				Map.Entry::getKey, 
				Map.Entry::getValue,
				(oldValue, newValue) -> oldValue,
				LinkedHashMap::new));
*/

		return 0;
	}

}

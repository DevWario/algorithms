package algorithms;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;


public class InsertionSortTest {

	@Test
	public void testarInverso() {
		InsertionSort sort = new InsertionSort();
		int[] sortedArray = sort.sort(new int[] {9,7, 5, 3, 1});
		System.out.println("Inverso: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,3, 5, 7, 9});
	}

	@Test
	public void testarEmbaralhadoNoFim() {
		InsertionSort sort = new InsertionSort();
		int[] sortedArray = sort.sort(new int[] {9,7, 1, 2, 3});
		System.out.println("No Fim: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,2, 3, 7, 9});
	}

	@Test
	public void testarEmbaralhadoNoMeio() {
		InsertionSort sort = new InsertionSort();
		int[] sortedArray = sort.sort(new int[] {9,1, 2, 3, 7});
		System.out.println("No Meio: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,2, 3, 7, 9});
	}

	@Test
	public void testarInversoOfficial() {
		InsertionSort sort = new InsertionSort();
		int[] sortedArray = sort.sortCorrect(new int[] {9,7, 5, 3, 1});
		System.out.println("Inverso: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,3, 5, 7, 9});
	}

	@Test
	public void testarEmbaralhadoNoFimOfficial() {
		InsertionSort sort = new InsertionSort();
		int[] sortedArray = sort.sortCorrect(new int[] {9,7, 1, 2, 3});
		System.out.println("No Fim: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,2, 3, 7, 9});
	}

	@Test
	public void testarEmbaralhadoNoMeioOfficial() {
		InsertionSort sort = new InsertionSort();
		int[] sortedArray = sort.sortCorrect(new int[] {9,1, 2, 3, 7});
		System.out.println("No Meio: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,2, 3, 7, 9});
 	}

}

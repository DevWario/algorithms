package algorithms;

import org.junit.jupiter.api.Test;

import java.util.Arrays;



public class RotateArrayTest {
	
    public void rotate(int[] nums, int indexRotation) {
        int[] array = new int[nums.length];
        
        for(int index = 0 ; index< nums.length; index++){
            array[(index + indexRotation) % nums.length ] = nums[index];
        }
        
        nums = array;
        System.out.println(Arrays.toString(nums));
    }

    @Test
	public void testName() throws Exception {
		int[] array = {1,2,3,4,5,6,7};
		
		rotate(array, 3);
	}

}

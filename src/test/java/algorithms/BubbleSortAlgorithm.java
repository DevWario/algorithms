package algorithms;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;


public class BubbleSortAlgorithm {
	
	public int[] sort(int[] array) {
		int end = array.length - 1;
		
		while(end > 0) {
			
			for(int i = 0; i < end; i++) {
				if(array[i] > array[i + 1]) {
					int temp = array[i];
					array[i] = array[i + 1];
					array[i + 1] = temp;
				}
			}
			
			end--;
		}
		return array;
	}
	
	@Test
	public void whenReceive54321Array_thenReturn12345Array() throws Exception {
		int[] expected = {1, 2, 3, 4, 5};
		int[] actual = sort(new int[] {5, 4, 3, 2, 1});
		
		System.out.println(Arrays.toString(actual));
		assertArrayEquals(expected, actual);
	}
	
	@Test
	public void whenReceive5432160Array_thenReturn0123456Array() throws Exception {
		int[] expected = {0, 1, 2, 3, 4, 5, 6};
		int[] actual = sort(new int[] {5, 4, 3, 2, 1, 6, 0});
		
		System.out.println(Arrays.toString(actual));
		assertArrayEquals(expected, actual);
	}
	
}

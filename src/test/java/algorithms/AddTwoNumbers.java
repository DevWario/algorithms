package algorithms;

import algorithms.structures.ListNode;

public class AddTwoNumbers {

	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		
		StringBuilder firstValue = new StringBuilder();
		StringBuilder secondValue = new StringBuilder();
		
		firstValue.append(l1.val);
		while(l1.next != null) {
			l1 = l1.next;
			firstValue.append(l1.val);
		}
		
		secondValue.append(l2.val);
		while(l2.next != null) {
			l2 = l2.next;
			secondValue.append(l2.val);
		}
		
		firstValue = firstValue.reverse();
		secondValue = secondValue.reverse();
		
		int resultado = Integer.parseInt(firstValue.toString()) + Integer.parseInt(secondValue.toString());
		
		StringBuilder resultadoString = new StringBuilder();
		resultadoString.append(resultado);
		String resultadoInverso = resultadoString.reverse().toString();
		char[] resultadoInversoChar = resultadoInverso.toCharArray();
		
		ListNode listResultado = new ListNode(Integer.parseInt(Character.toString(resultadoInversoChar[0])));
		for (int i = 1; i < resultadoInversoChar.length; i++) {
			ListNode listResultadoNext = new ListNode(Integer.parseInt(Character.toString(resultadoInversoChar[i])));
			
			listResultado.next = listResultadoNext;
		}
		
		return listResultado;
	}

}

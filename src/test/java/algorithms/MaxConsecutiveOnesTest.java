package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MaxConsecutiveOnesTest {
	
	@Test
	public void testMaxInBegin() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnes(new int[] {1,1,1,0,1,1,0,1});
		assertEquals(3, maxConsecutive);
	}

	@Test
	public void testMaxInEnd() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnes(new int[] {1,1,1,0,1,1,0,1,1,1,1});
		assertEquals(4, maxConsecutive);
	}

	@Test
	public void testMaxInMiddle() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesCustomized(new int[] {1,1,1,0,1,1,1,1,1,0,1,1,1,1});
		assertEquals(5, maxConsecutive);
	}

	@Test
	public void testMaxInBeginCustomized() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesCustomized(new int[] {1,1,1,0,1,1,0,1});
		assertEquals(3, maxConsecutive);
	}

	@Test
	public void testMaxInEndCustomized() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesCustomized(new int[] {1,1,1,0,1,1,0,1,1,1,1});
		assertEquals(4, maxConsecutive);
	}

	@Test
	public void testMaxInMiddleCustomized() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesCustomized(new int[] {1,1,1,0,1,1,1,1,1,0,1,1,1,1});
		assertEquals(5, maxConsecutive);
	}

	@Test
	public void testMaxInOptimized1101Customized() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesCustomized(new int[] {1,1,0,1});
		assertEquals(2, maxConsecutive);
	}

	@Test
	
	public void testMaxInOptimized1Customized() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesCustomized(new int[] {1});
		assertEquals(1, maxConsecutive);
	}

	@Test
	//
	public void testMaxInBeginOptimized() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesOptimized(new int[] {1,1,1,0,1,1,0,1});
		assertEquals(3, maxConsecutive);
	}

	@Test
	//
	public void testMaxInEndOptimized() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesOptimized(new int[] {1,1,1,0,1,1,0,1,1,1,1});
		assertEquals(4, maxConsecutive);
	}

	@Test
	//
	public void testMaxInMiddleOptimized() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesOptimized(new int[] {1,1,1,0,1,1,1,1,1,0,1,1,1,1});
		assertEquals(5, maxConsecutive);
	}

	@Test
	//
	public void testMaxInOptimized1101() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesOptimized(new int[] {1,1,0,1});
		assertEquals(2, maxConsecutive);
	}

	@Test
	public void testMaxInOptimized1() {
		MaxConsecutiveOnes max = new MaxConsecutiveOnes();
		int maxConsecutive = max.getMaxConsecutiveOnesOptimized(new int[] {1});
		assertEquals(1, maxConsecutive);
	}

}

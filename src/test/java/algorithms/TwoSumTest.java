package algorithms;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TwoSumTest {

	@Test
	public void testarDesordenado() {
		TwoSum twoSum  = new TwoSum();
		int[] indexResult = twoSum.sum(new int[] {3, 2, 4},6);
		assertArrayEquals(indexResult, new int[] {1,2});
	}

	@Test
	public void testarIguais() {
		TwoSum object  = new TwoSum();
		int[] indexResult = object.sum(new int[] {3, 3},6);
		assertArrayEquals(indexResult, new int[] {0,1});
	}

}

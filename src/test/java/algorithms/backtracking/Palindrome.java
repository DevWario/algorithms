package algorithms.backtracking;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class Palindrome {
	
	private void permutation(int[] input, List<Integer> partial, boolean[] used ) {
	
		if(partial.size() == input.length) {
			System.out.println(Arrays.toString(partial.toArray()));
			return;
		}
	
		for(int i = 0; i < input.length; i++) {
			if(!used[i]) {
				used[i] = true;
				partial.add(input[i]);
				permutation(input, partial, used);
				used[i] = false;
				partial.remove(partial.size() - 1);
			}
		}
	}
	
	@Test
	public void testPermutation() {
		int[] input = {1,2,3};
		List<Integer>partial = new ArrayList<>();
		boolean[] used = {false, false, false};
		
		permutation(input, partial, used);
	}

}

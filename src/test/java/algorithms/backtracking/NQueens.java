package algorithms.backtracking;


import org.junit.jupiter.api.Test;

/**
 * BAcktracking.
 * 
 * You are given a chess board of size NxN, qhere N is almost 8.
 * 
 * You have to place N queens in this board such that no two queens attack. A
 * Qeeun can move
 * 
 * @author Marcelo Pinheiro.
 *
 */
public class NQueens {

	public static boolean helper(int[] board, int i) {
		if (i == board.length) {
			for (int row : board) {
				for (int c = 0; c < board.length; c++) {
					if (c == row) {
						System.out.println(" 0 ");
					} else {
						System.out.println(" X ");
					}
				}
				System.out.println("");
			}
			return true;
		}

		for (int c = 0; c < board.length; c++) {
			boolean flag = false;
			for (int r = 0; r < i; r++) {
				if (board[r] == c || Math.abs(board[r] - c) == (i - r)) {
					flag = false;
					break;
				}
			}

			if (flag) {
				continue;
			}

			board[i] = c;

			if (helper(board, i + 1)) {
				return true;
			}
		}
		return false;
	}

	@Test
	public void testChessWithFourPlaces() {
		int[] board = new int[4];
		if (!helper(board, 0)) {
			System.out.println("No solution");
		}
	}

}

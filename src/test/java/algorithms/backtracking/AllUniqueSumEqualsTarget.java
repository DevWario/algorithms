package algorithms.backtracking;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



/**
 * Backtracking.
 * 
 * Given a list of numbers, and a target number, print all the unique combinations 
 * in candidates numbers sum to target.
 * </br>
 * Input = [10, 1, 2, 7, 6, 1, 5], target = 8
 * </br>
 * Output = [1, 1, 6], [1, 2, 5], [1, 7], [2, 6]
 * </br>
 * 
 * @author Marcelo Pinheiro
 *
 */
public class AllUniqueSumEqualsTarget {
	
	private static void combinationSum(int[] nums, int target) {
		Arrays.sort(nums);
		combinationSum(nums, target, 0, new ArrayList<>(), 0);
	}
	
	private static void combinationSum(int[] nums, int target, int sum, List<Integer> partial, int start) {
		if(sum == target) {
			System.out.println(Arrays.toString(partial.toArray()));
			return;
		}
		
		for (int i = start; i < nums.length; i++) {
			int c = nums[i];
			
			if(sum + c > target || i > start && nums[i] == nums[i - 1]) {
				continue;
			}
			
			partial.add(c);
			
			combinationSum(nums, target, sum + c, partial, i + 1);
			
			partial.remove(partial.size() - 1);
			
		}
	}
	
	@Test
	public void testCombinationSum() {
		combinationSum(new int[] {10, 1, 2, 7, 6, 1, 5}, 8);
	}
	

}

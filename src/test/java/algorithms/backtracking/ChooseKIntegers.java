package algorithms.backtracking;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;



/**
 * Backtracking.
 * 
 * Given an input array and an integer 'K' which is almost the size of the array,
 * generate all the ways we can choose K integers from the array.
 * 
 * @author Marcelo Pinheiro
 * 
 */
public class ChooseKIntegers {
	
	public static void combinationAlternative(int[] input, int k, Set<Integer> partial, int start /*index*/) {
		if(partial.size() == k) {
			System.out.println(Arrays.toString(partial.toArray()));
			return;
		}
		if(start == input.length) return;
		
		partial.add(input[start]);
		combinationAlternative(input, k, partial, start + 1);
		partial.remove(input[start]);
		combinationAlternative(input, k, partial, start + 1);
	}	
	
	public static void combination(int[] input, int k, Set<Integer> partial, int start /*index*/) {
		if(partial.size() == k) {
			System.out.println(partial);
			return;
		}
		
		if(start == input.length) return;
		
		for(int i = start; i < input.length; i++) {
			partial.add(input[i]);
			combination(input, k, partial, i+1);
			partial.remove(input[i]);
		}
	}
	
	@Test
	public void testCombination() {
		int[] input = {3, 2, 5, 8};
		int k = 3;
		Set<Integer> set = new HashSet<>();
		int start = 0;
		
		combination(input, k, set, start);
	}
	
	@Test
	public void testCombinationAlternative() {
		int[] input = {3, 2, 5, 8};
		int k = 3;
		Set<Integer> set = new HashSet<>();
		int start = 0;
		
		combinationAlternative(input, k, set, start);
	}

}

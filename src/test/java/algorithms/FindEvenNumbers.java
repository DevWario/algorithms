package algorithms;


import org.junit.jupiter.api.Test;

public class FindEvenNumbers {

	public int findNumbers(int[] nums) {
		int count = 0;
		for (int i = 0; i < nums.length; i++) {
			int quantity = getQuantityAlgarism(nums[i]);
			if (quantity % 2 == 0) {
				count++;
			}
		}

		return count;
	}

	private int getQuantityAlgarism(int num) {
		int count = 0;
		while (num  > 0) {
			count++;
			num /= 10;
		}
		return count;
	}

	@Test
	public void testName() throws Exception {
		int[] nums = { 0, 10, 3000, 423833, 4444444 };
		System.out.println(findNumbers(nums));

	}

}

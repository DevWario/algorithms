package algorithms;

import org.junit.jupiter.api.Test;

import java.util.Arrays;



public class SegregateEvenOddInArrayTest {

	@Test
	public void testRandomArray() {
		int[] randomArray = {1,2,3,4,5,6,7,87,9,90};
		SegregateEvenOddInArray.segregate(randomArray);
		System.out.println(Arrays.toString(randomArray));
		
	}

	@Test
	public void testRandomEqualsSampleArray() {
		int[] randomArray = {12,34,45,9,8,90, 3};
		SegregateEvenOddInArray.segregate(randomArray);
		System.out.println(Arrays.toString(randomArray));
		
	}

	@Test
	public void testRandomEqualsArray() {
		int[] randomArray = {90,90,45,9,8,90, 3};
		SegregateEvenOddInArray.segregate(randomArray);
		System.out.println(Arrays.toString(randomArray));
		
	}

}

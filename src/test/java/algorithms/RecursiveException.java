package algorithms;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RecursiveException {
	
	public Throwable getException(Throwable throwable){
		if(throwable == null) {
			return null;
		}
		
		if(throwable instanceof SQLException) {
			return throwable;
		} 
		
		return getException(throwable.getCause());
	}
	
	@Test
	public void whenUseNullThrowable_returnNull() throws Exception {
		Exception exception = null;
		Throwable expected = null;
		Throwable actual = getException(exception);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void whenUseAnotherThrowable_returnNull() throws Exception {
		Exception exception = new Exception();
		Throwable expected = null;
		Throwable actual = getException(exception);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void whenUseTwoLevelsThrowable_returnNull() throws Exception {
		Exception exception1 = new Exception();
		Exception exception2 = new Exception(exception1);
		Throwable expected = null;
		Throwable actual = getException(exception2);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void whenUseThreeLevelsWithThrowable_returnThrowable() throws Exception {
		Exception exception1 = new SQLException();
		Exception exception2 = new Exception(exception1);
		Exception exception3 = new Exception(exception2);
		Throwable expected = exception1;
		Throwable actual = getException(exception3);
		
		assertEquals(expected, actual);
	}

}

package algorithms;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;


public class SelectionSortAgain {
	
	public int[] sort(int[] array) {
		int lastUnorderedIndex = array.length - 1;
		int maxIndex = 0;
		
		while(lastUnorderedIndex > 0) {
			
			for(int i = 1; i <= lastUnorderedIndex; i++) {
				if(array[i] > array[maxIndex]) {
					maxIndex = i;
				}
			}
			
			int temp = array[lastUnorderedIndex];
			array[lastUnorderedIndex] = array[maxIndex];
			array[maxIndex] = temp;
			
			lastUnorderedIndex--;
			maxIndex = 0;
		}
		
		return array;
	}
	
	@Test
	public void whenReceived54321_thenReturn12345() throws Exception {
		int[] expected = {1, 2, 3, 4, 5};
		int[] actual = sort(new int[] {5, 4, 3, 2, 1});
		
		System.out.println(Arrays.toString(actual));
		assertArrayEquals(expected, actual);
	}

	@Test
	public void whenReceive5432160Array_thenReturn0123456Array() throws Exception {
		int[] expected = {0, 1, 2, 3, 4, 5, 6};
		int[] actual = sort(new int[] {5, 4, 3, 2, 1, 6, 0});
		
		System.out.println(Arrays.toString(actual));
		assertArrayEquals(expected, actual);
	}
	

}

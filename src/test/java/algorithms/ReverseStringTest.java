package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReverseStringTest {
	
	ReverseString reverser = new ReverseString();
	
	@Test
	public void testHappyDay() {
		String original = "marcelo";
		String expected = "olecram";
		String actual = reverser.reverse(original);
		assertEquals(expected, actual);
	}
	
	@Test 
	public void testPalindrome() {
		String original = "arara";
		String expected = "arara";
		String actual = reverser.reverse(original);
		assertEquals(expected, actual);
	}
	
	@Test 
	public void testEmpty() {
		String original = "";
		String expected = "";
		String actual = reverser.reverse(original);
		assertEquals(expected, actual);
	}

}

package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.Arrays;



public class MySelectionSortTest {

	@Test
	public void testarInverso() {
		MySelectionSort sort = new MySelectionSort();
		int[] sortedArray = sort.sort(new int[] {9,7, 5, 3, 1});
		System.out.println("Inverso: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,3, 5, 7, 9});
	}

	@Test
	public void testarEmbaralhadoNoFim() {
		MySelectionSort sort = new MySelectionSort();
		int[] sortedArray = sort.sort(new int[] {9,7, 1, 2, 3});
		System.out.println("No Fim: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,2, 3, 7, 9});
	}

	@Test
	public void testarEmbaralhadoNoMeio() {
		MySelectionSort sort = new MySelectionSort();
		int[] sortedArray = sort.sort(new int[] {9,1, 2, 3, 7});
		System.out.println("No Meio: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,2, 3, 7, 9});
	}

}

package algorithms;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PrefixSum{

	public long prefixSum(int[] array) {
		long sum = array[0];
		for (int i = 1; i < array.length; i++) {
			sum += array[i];
		}
		return sum;
	}

}

public class PrefixSumTest {

	@Test
	public void whenReceive01234_thenReturn10(){
		int[] array = {0,1,2,3,4};
		
		System.out.println("strawberries".substring(2, 5));
		PrefixSum prefixSum = new PrefixSum();
		long result = prefixSum.prefixSum(array);
		
		assertEquals(10L, result);
	}
	

	@Test
	public void whenReceive63Minus24Minus10Minus5_thenReturn5(){
		System.out.println("apple".compareTo("banana"));
		int[] array = {6, 3, -2, 4, -1, 0, -5};
		
		PrefixSum prefixSum = new PrefixSum();
		long result = prefixSum.prefixSum(array);
		
		assertEquals(5L, result);
	}
	
}

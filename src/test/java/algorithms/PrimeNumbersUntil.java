package algorithms;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrimeNumbersUntil {

    @Test
    public void testPrimeNumbersUntilNaive3(){
        List<Integer> expect = new ArrayList<Integer>();
        expect.add(2);
        expect.add(3);
        List<Integer> result = primeNumberUntil(3);
        assertEquals(expect, result);
    }

    @Test
    public void testPrimeNumbersUntilNaive5(){
        List<Integer> expect = new ArrayList<Integer>();
        expect.add(2);
        expect.add(3);
        expect.add(5);
        List<Integer> result = primeNumberUntil(5);
        assertEquals(expect, result);
    }

    @Test
    public void testPrimeNumbersUntilNaive30(){
        List<Integer> expect = Arrays.asList(2, 3, 5, 7, 11, 13 ,17 ,19 ,23, 29);
        List<Integer> result = primeNumberUntil(30);
        assertEquals(expect, result);
    }

    private List<Integer> primeNumberUntil(int number) {
        List<Integer> result = new ArrayList<Integer>();
        for(int i = 2; i <= number; i++){
            if(isPrimeNaive(i)){
                result.add(i);
            }
        }
        return result;
    }

    private boolean isPrimeNaive(int i) {
        boolean isPrime = true;
        int j = i;
        while(j > 1){
            if(i % j == 0 && i != j){
                isPrime = false;
            }
            j--;
        }
        return isPrime;
    }

    private List<Integer> getPrimeList(int number) {
        boolean[] primes = new boolean[number];
        for (int i = 0; i < number; i++){
            primes[i] = true;
        }

        for (int i = 2; i <= number; i++) {
            for(int j = i*i; i<number; j+=i){
                primes[j] = false;
            }
        }

        return new ArrayList<>();
    }

}

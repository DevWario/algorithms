package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FindMaxSubArray{
	public static int find(int[] array, int k) {
		return array[0];
	}
}

public class FindMaxSubArrayTest {
	
	@Test
	public void testName() throws Exception {
		int[] array = {1,-1,5,-2,3};
		int k = 3;
		int result = FindMaxSubArray.find(array, k);
		
		assertEquals(4,  result);
		
	}
}

package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TestFindMaximumThreeNumbers {
	
	@Test
	public void testArrayCopy() {
		int[] array = new int[]{1, 5 , 3 , 7 , 9 };
		ArrayCopy max = new ArrayCopy();
		int[] arrayCopy = max.copy(array);
		assertArrayEquals(array, arrayCopy);
		
	}

}

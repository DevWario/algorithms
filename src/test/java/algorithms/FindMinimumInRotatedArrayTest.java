package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FindMinimumInRotatedArrayTest {
	
	FindMinimumInRotatedArray find = new FindMinimumInRotatedArray();
	
	@Test
	public void findMinimumHappyDay() {
		int[] rotatedArray = {1,2,3,4,5,6};
		int result = find.findMinimum(rotatedArray);
		assertEquals(1, result);
	}

	@Test
	public void findMinimumRotated() {
		int[] rotatedArray = {2,3,4,5,6,1};
		int result = find.findMinimum(rotatedArray);
		assertEquals(1, result);
	}

}

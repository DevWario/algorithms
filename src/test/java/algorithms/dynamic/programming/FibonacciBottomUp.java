package algorithms.dynamic.programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FibonacciBottomUp {

    @Test
    public void testFibonacciBottomUp0(){
        assertEquals(0, fibonnacciBottomUp(0));
    }

    @Test
    public void testFibonacciBottomUp1(){
        assertEquals(1, fibonnacciBottomUp(1));
    }

    @Test
    public void testFibonacciBottomUp2(){
        assertEquals(1, fibonnacciBottomUp(2));
    }

    @Test
    public void testFibonacciBottomUp3(){
        assertEquals(2, fibonnacciBottomUp(3));
    }

    @Test
    public void testFibonacciBottomUp4(){
        assertEquals(3, fibonnacciBottomUp(4));
    }

    @Test
    public void testFibonacciBottomUp5(){
        assertEquals(5, fibonnacciBottomUp(5));
    }

    private int fibonnacciBottomUp(int number){
        if(number == 0) return 0;

        int[] memo = new int[number + 1];
        memo[0] = 0;
        memo[1] = 1;

        for(int i = 2; i <= number; i++){
            memo[i] = memo[i - 1] + memo[i - 2];
        }

        return memo[number];

    }
}

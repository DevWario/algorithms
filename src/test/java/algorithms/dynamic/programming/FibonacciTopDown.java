package algorithms.dynamic.programming;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FibonacciTopDown {

    private Map<Integer, Integer> memo = new HashMap<>();

    @Test
    public void testFibonacciTopDown0(){
        assertEquals(0, fibonnacciTopDown(0));
    }

    @Test
    public void testFibonacciTopDown1(){
        assertEquals(1, fibonnacciTopDown(1));
    }

    @Test
    public void testFibonacciTopDown2(){
        assertEquals(1, fibonnacciTopDown(2));
    }

    @Test
    public void testFibonacciTopDown3(){
        assertEquals(2, fibonnacciTopDown(3));
    }

    @Test
    public void testFibonacciTopDown4(){
        assertEquals(3, fibonnacciTopDown(4));
    }

    @Test
    public void testFibonacciTopDown5(){
        assertEquals(5, fibonnacciTopDown(5));
    }

    private int fibonnacciTopDown(int number){

        if(memo.containsKey(number)){
            return memo.get(number);
        }

        if(number == 0) return 0;
        if(number == 1) return 1;

        int value = fibonnacciTopDown(number - 1) + fibonnacciTopDown(number - 2);
        memo.put(number, value);
        return value;
    }
}

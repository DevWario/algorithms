package algorithms.dynamic.programming;

import org.junit.jupiter.api.Test;

public class CuttingRodBottomUpNaive {

    @Test
    public void testCuttingRodNaiveWay(){
        int prices[] = new int[] { 1, 5, 8, 9, 10, 17, 17, 20 };
        //cutRod(n) = max(price[i] + cutRod(n-i-1)) for all i in {0, 1 .. n-1}
        System.out.println(maxCuttingRodNaive(prices, prices.length));
    }

    private int maxCuttingRodNaive(int[] prices, int numberOfPieces){
        int[] memo = new int[prices.length + 1];
        for (int value: memo) value = Integer.MIN_VALUE;
        memo[0] = 0;
        int max = memo[0];

        for(int index = 1; index <= numberOfPieces; index++){
            for(int j = 0; j < index; j++)
                max = Math.max(max, prices[j] + memo[index - j - 1]);
            memo[index] = max;
        }
        return max;
    }
}

package algorithms.dynamic.programming;

import org.junit.jupiter.api.Test;

public class CuttingRodTopDown {

    @Test
    public void testCuttingRod(){
        int prices[] = new int[] { 1, 5, 8, 9, 10, 17, 17, 20 };
        //cutRod(n) = max(price[i] + cutRod(n-i-1)) for all i in {0, 1 .. n-1}
        System.out.println(maxCuttingRod(prices, prices.length));
    }

    private int maxCuttingRod(int[] prices, int numberOfPieces){
        int[] memo = new int[numberOfPieces + 1]; //It has one more item because contains the 0 case to stop recursion.
        for(int item : memo) item = Integer.MIN_VALUE; //Initialize the memoization of the sums.
        return cuttingRod(prices, prices.length, memo); //Subproblem
    }

    private int cuttingRod(int[] prices, int numberOfPieces, int[] memo){
        int max;
        if(numberOfPieces == 0){
            max = 0;
        }
        else{
            max = Integer.MIN_VALUE;
            for(int index = 0; index < numberOfPieces; index++){
                max = Math.max(max, prices[index] + cuttingRod(prices, numberOfPieces - index - 1, memo));
            }
        }
        memo[numberOfPieces] = max;
        return memo[numberOfPieces];
    }
}

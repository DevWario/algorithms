package algorithms.dynamic.programming;

import org.junit.jupiter.api.Test;

public class CuttingRodTopDownNaive {

    @Test
    public void testCuttingRodNaiveWay(){
        int prices[] = new int[] { 1, 5, 8, 9, 10, 17, 17, 20 };
        //cutRod(n) = max(price[i] + cutRod(n-i-1)) for all i in {0, 1 .. n-1}
        maxCuttingRodNaive(prices, prices.length);
    }

    private int maxCuttingRodNaive(int[] prices, int numberOfPieces){
        if (numberOfPieces == 0) return 0;
        int max = Integer.MIN_VALUE;
        for(int index = 0; index < numberOfPieces; index++){
            max = Math.max(max, prices[index] + maxCuttingRodNaive(prices, numberOfPieces - index - 1));
        }
        return max;
    }
}

package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;


class PrefixSumArray {
	
	public long[] getPrefixSumArray(long[] array) {
		long[] arraySum = new long[array.length];
		arraySum[0] = array[0];
		for (int i = 1; i < array.length; i++) {
			arraySum[i] = arraySum[i - 1] + array[i]; 
		}
		return arraySum;
	}
	
}

public class PrefixSumArrayTest {
	
	@Test
	public void whenArray4257_thenReturn461118() throws Exception {
		long[] array = {4L, 2L, 5L, 7L};
		long[] sumArray = new PrefixSumArray().getPrefixSumArray(array);
		assertArrayEquals(new long[] {4L, 6L, 11L, 18L}, sumArray);
	}


}

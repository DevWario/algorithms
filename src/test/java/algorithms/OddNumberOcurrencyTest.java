package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OddNumberOcurrencyTest {
	OddNumberOcurrency oddNumberOcurrency = new OddNumberOcurrency();

	@Test
	public void testSimpleArray() {
		int[] array = {1,1,2,2,2,3,3};
		int actual = oddNumberOcurrency.find(array);
		assertEquals(2, actual);
		
	}

}

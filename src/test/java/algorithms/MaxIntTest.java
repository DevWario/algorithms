package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MaxIntTest {
	
	class MaxChar {
		public char getMaxChar(String string) {
			return (char)string.chars()
				.reduce(Integer.MIN_VALUE, 
						(max, actual) -> actual > max ? actual : max);
		}
	}
	
	MaxChar maxChar = new MaxChar();
	
	@Test
	public void testAbccccccd() {
		String origin = "abcccccd";
		char expected = 'd';
		char actual = maxChar.getMaxChar(origin);
		assertEquals(expected, actual);
		
	}

	@Test 
	public void testZAbccccccd() {
		String origin = "Zabcccccd";
		char expected = 'Z';
		char actual = maxChar.getMaxChar(origin);
		assertEquals(expected, actual);
		
	}


}

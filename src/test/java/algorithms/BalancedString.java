package algorithms;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BalancedString {

    public int balancedStringSplit(String s) {
    	int balance = 0;
    	int count = 0;
    	
    	char[] characters = s.toCharArray();
    	
    	for (int i = 0; i < characters.length; i++) {
			if(characters[i] == 'R') {
				balance++;
			} else {
				balance--;
			}
			
			if(balance == 0) {
				count++;
			}
		}
    	
        return count;
    }
    
    @Test
	public void testName() throws Exception {
		int result = balancedStringSplit("RLRRLLRLRL");
		assertEquals(4, result);
	}
    
    

}

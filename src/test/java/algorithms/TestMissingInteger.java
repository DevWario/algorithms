package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMissingInteger {

	public TestMissingInteger() {
		// TODO Auto-generated constructor stub
	}

	@Test
	public void testPositives() {
		int[] array = new int[] {1, 3, 6, 4, 1, 2};
		Solution missing = new Solution();
		int solution = missing.solution(array);
		assertEquals(5, solution);
	}

	@Test
	public void testNegatives() {
		int[] array = new int[] {-1, -3};
		Solution missing = new Solution();
		int solution = missing.solution(array);
		assertEquals(1, solution);
	}

	@Test
	public void testNegativesAndOne() {
		int[] array = new int[] {-1, -3 , 1};
		Solution missing = new Solution();
		int solution = missing.solution(array);
		assertEquals(2, solution);
	}

	@Test
	public void testNegatives123() {
		int[] array = new int[] {3, 2 , 1};
		Solution missing = new Solution();
		int solution = missing.solution(array);
		assertEquals(4, solution);
	}

	@Test
	public void testExtremes() {
		int[] array = new int[] {-111111, 999999};
		Solution missing = new Solution();
		int solution = missing.solution(array);
		assertEquals(1, solution);
	}
	
	
	
	
}

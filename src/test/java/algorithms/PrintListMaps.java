package algorithms;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrintListMaps {

    @Test
    public void testPrintListMap(){
        List<Map<String, String>> structure = getStructure();
        structure.stream()
                .peek(it -> System.out.println("-------------------------"))
                .flatMap(it -> it.entrySet().stream())
                .forEach(it -> {
                    System.out.print(it.getKey());
                    System.out.print(": ");
                    System.out.println(it.getValue());
                });

    }

    private List<Map<String, String>> getStructure() {
        List<Map<String, String>> structure = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("Name","TextFirst1");
        map.put("Address","TextFirst2");
        map.put("Phone Number","TextFirst3");
        map.put("BirthDay","TextFirst4");
        map.put("Email","TextFirst5");
        structure.add(map);
        map = new HashMap<>();
        map.put("Name","TextSecond1");
        map.put("BirthDay","TextSecond4");
        map.put("Email","TextSecond5");
        structure.add(map);
        map = new HashMap<>();
        map.put("Name","TextThird1");
        map.put("Address","TextThird2");
        map.put("Phone Number","TextThird3");
        map.put("Email","TextThird5");
        structure.add(map);
        return structure;
    }

}

package algorithms.arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.function.Function;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OddOccurrencesInArray {
	
	public int solution(int[] A) {
		return Arrays.stream(A)
				.boxed()
				.collect(groupingBy(Function.identity(), counting()))
				.entrySet()
				.stream()
				.filter(entry -> entry.getValue() == 1)
				.peek(System.out::println)
				.findFirst()
				.map(entry -> entry.getKey())
				.get();
	}

	@Test
	public void test2001() {
		int[] A = {2001};
		int actual = solution(A);
		assertEquals(2001, actual);
		
	}

	@Test
	public void test201() {
		int[] A = {3,3,201, 202, 202, 1,1, 14, 16, 14 ,16};
		int actual = solution(A);
		assertEquals(3, actual);
		
	}
}

package algorithms.arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Equilibrium {

    public int solutionOptimized(int[] array){
        int sumLeft = array[0];
        int sumRight = 0;
        int min = Integer.MAX_VALUE;

        for(int i = 1; i < array.length; i++){
            sumRight += array[i];
        }

        min = Math.min(Math.abs(sumLeft - sumRight), min);

        for(int i = 1; i< array.length - 1; i++){
            sumLeft += array[i];
            sumRight -= array[i];
            min = Math.min(Math.abs(sumLeft - sumRight), min);
        }

        return min;

    }

    public int solution(int[] array){
        int[] arrayLeft = new int[array.length - 1];
        int[] arrayRight = new int[array.length - 1];
        int sumLeft = 0;
        int sumRight = 0;

        for(int i = 0; i < array.length - 1; i++){
            sumLeft += array[i];
            arrayLeft[i] = sumLeft;
        }

        for(int i = array.length - 1; i > 0; i--){
            sumRight += array[i];
            arrayRight[i - 1] = sumRight;
        }

        int min = Integer.MAX_VALUE;
        for(int i = 0; i < arrayLeft.length; i++){
            int difference = Math.abs(arrayLeft[i] - arrayRight[i]);
            min = Math.min(min, difference);
        }
        return min;
    }

    @Test
    public void given31243Than1(){
        int[] array = {3,1,2,4,3};
        int result = solution(array);
        assertEquals(1,result);
    }

    @Test
    public void given3Than1Optimized(){
        int[] array = {3,1,2,4,3};
        int result = solutionOptimized(array);
        assertEquals(1,result);
    }
}

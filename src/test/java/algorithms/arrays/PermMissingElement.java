package algorithms.arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PermMissingElement {

    public int findMissingElementONLogN(int[] array){
        Arrays.sort(array);
        for(int i = 1; i < array.length; i++){
            if(array[i] - array[i - 1] != 1){
                return array[i - 1] + 1;
            }
        }
        return -1;
    }

    public int findMissingElementON(int[] array){
        int sum = 0;
        int sumMissing = 0;
        for(int i = 1; i <= array.length + 1; i++){
            sum += i;
        }
        System.out.println(sum);
        for(int i = 0; i < array.length; i++){
            sumMissing += array[i];
        }
        return sum - sumMissing;
    }

    public int findMissingElementONOptimized(int[] array){
        int size = array.length + 1;
        int sum = (size * (size + 1))/2;
        int sumMissing = 0;

        for(int i = 0; i < array.length; i++){
            sumMissing += array[i];
        }
        return sum - sumMissing;
    }

    @Test
    public void given2315Than4OnLogNOptimized(){
        int[] array = {2,3,1,5};
        int result = findMissingElementONOptimized(array);
        assertEquals(4,result);
    }

    @Test
    public void given2315Than4OnLogN(){
        int[] array = {2,3,1,5};
        int result = findMissingElementONLogN(array);
        assertEquals(4,result);
    }

    @Test
    public void given2315Than4On(){
        int[] array = {2,3,1,5};
        int result = findMissingElementON(array);
        assertEquals(4,result);
    }
}

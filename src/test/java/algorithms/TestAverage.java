package algorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestAverage {
	
	@Test
	public void testAverage() {
		int[] array = new int[]{1, 5 , 3 , 7 , 9 };
		Average average = new Average();
		int averageNumber = average.calculateAvarage(array);
		assertEquals(averageNumber, 5);
		
	}

	@Test
	@DisplayName("Test the algorithm with negative numbers")
	public void testAverageNumbersWithNegativeNumbers() {
		int[] array = new int[]{1, -5 , 3 , 11 , -5 };
		Average average = new Average();
		int averageNumber = average.calculateAvarage(array);
		assertEquals(averageNumber, 1);
		
	}

}

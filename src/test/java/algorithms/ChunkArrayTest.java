package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ChunkArray {

	public int[][] chunkArray(int[] array, int chunkSize) {
		int arraySize = (array.length / chunkSize) + 1;
		int[][] checkedArray = new int[arraySize][];
		
		for (int i = 0; i < array.length; i++) {
			//checkedArray = new int[][]
			System.out.println((array.length + i) % chunkSize);
		}
		return null ;
	}
	
}

public class ChunkArrayTest {
	
	ChunkArray chunk = new ChunkArray();
	
	@Test
	public void testChunk1() {
		int[][] expected = {{1, 2},{3, 4}};
		int[] array = {1, 2, 3, 4};
		int chunkSize = 2;
		int[][] actual = chunk.chunkArray(array, chunkSize);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChunk2() {
		int[][] expected = {{1, 2},{3, 4},{5}};
		int[] array = {1, 2, 3, 4, 5};
		int chunkSize = 2;
		int[][] actual = chunk.chunkArray(array, chunkSize);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChunk3() {
		int[][] expected = {{1, 2, 3},{4, 5, 6}, {7, 8}};
		int[] array = {1, 2, 3, 4, 5, 6, 7, 8};
		int chunkSize = 3;
		int[][] actual = chunk.chunkArray(array, chunkSize);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChunk4() {
		int[][] expected = {{1, 2, 3, 4},{5}};
		int[] array = {1, 2, 3, 4, 5};
		int chunkSize = 4;
		int[][] actual = chunk.chunkArray(array, chunkSize);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChunk5() {
		int[][] expected = {{1, 2, 3, 4, 5}};
		int[] array = {1, 2, 3, 4, 5};
		int chunkSize = 10;
		int[][] actual = chunk.chunkArray(array, chunkSize);
		assertEquals(expected, actual);
	}

}

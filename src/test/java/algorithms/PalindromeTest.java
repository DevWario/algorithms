package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PalindromeTest {
	
	Palindrome palindrome = new Palindrome();
	
	@Test
	public void testPalindrome() {
		String word = "socorrammesubinoonibusemmarrocos";
		assertTrue(palindrome.isPalindrome(word));
	}
	
	@Test
	public void testNotPalindrome() {
		String word = "socorrammesubinooanibusemmarrocos";
		assertFalse(palindrome.isPalindrome(word));
	}

}

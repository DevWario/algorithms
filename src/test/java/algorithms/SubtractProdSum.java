package algorithms;


import org.junit.jupiter.api.Test;

public class SubtractProdSum {
    public int subtractProductAndSum(int n) {
    	
    	int product = 1;
    	int sum = 0;
    	
    	while(n > 0) {
    		System.out.println(n % 10);
    		int value = n /= 10;
    		product *= value;
    		sum += value;
    	}
        return product - sum;
    }
    
    @Test
	public void testName() throws Exception {
		subtractProductAndSum(234);
	}
}

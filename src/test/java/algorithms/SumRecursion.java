package algorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SumRecursion {
	
	private int sum(int number) {
		if(number <= 0) {
			return 0;
		}
		int rest = number % 10;
		int percent = number / 10;
		return rest + sum(percent);
	}
	
	public void testSumOK() {
		int result = sum(123456789);
		assertEquals(45, result);
	}
	
	public void testSumKO() {
		int result = sum(2345678);
		assertEquals(35, result);
	}

}

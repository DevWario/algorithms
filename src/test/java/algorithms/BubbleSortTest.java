package algorithms;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class BubbleSortTest {

	@Test
	public void testarInverso() {
		BubbleSortAlgorithm sort = new BubbleSortAlgorithm();
		int[] sortedArray = sort.sort(new int[] {9,7, 5, 3, 1});
		System.out.println("Inverso: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,3, 5, 7, 9});
	}

	@Test
	public void testarEmbaralhadoNoFim() {
		BubbleSortAlgorithm sort = new BubbleSortAlgorithm();
		int[] sortedArray = sort.sort(new int[] {9,7, 1, 2, 3});
		System.out.println("No Fim: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,2, 3, 7, 9});
	}

	@Test
	public void testarEmbaralhadoNoMeio() {
		BubbleSortAlgorithm sort = new BubbleSortAlgorithm();
		int[] sortedArray = sort.sort(new int[] {9,1, 2, 3, 7});
		System.out.println("No Meio: "+Arrays.toString(sortedArray));
		assertArrayEquals(sortedArray,new int[] {1,2, 3, 7, 9});
	}

}

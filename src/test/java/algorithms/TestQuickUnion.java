package algorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import algorithms.structures.QuickUnionUF;
import org.junit.jupiter.api.Test;

public class TestQuickUnion {
	
	@Test
	public void testHappyDay() {
		QuickUnionUF quick = new QuickUnionUF(3);
		quick.union(0, 1);
		assertEquals(quick.connected(0, 1), true);
	}

	@Test
	public void testDoubleConnection() {
		QuickUnionUF quick = new QuickUnionUF(4);
		quick.union(0, 1);
		quick.union(1, 2);
		assertEquals(quick.connected(0, 2), true);
	}

	@Test
	public void testNotConnection() {
		QuickUnionUF quick = new QuickUnionUF(4);
		quick.union(0, 1);
		quick.union(1, 2);
		assertEquals(quick.connected(0, 3), false);
	}

}

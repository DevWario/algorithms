package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReverseLive {
	
	public String reverse(String string) {
		char[] charArray = string.toCharArray();
		int start = 0;
		int end = charArray.length - 1;
		
		while(start < end) {
			char temp = charArray[start];
			charArray[start] = charArray[end];
			charArray[end] = temp;
			
			start++;
			end--;
		}
		
		return new String(charArray);
		
	}
	
	@Test
	public void testName() throws Exception {
		String result = reverse("name");
		assertEquals("eman", result);
	}
	
	@Test
	public void testNames() throws Exception {
		String result = reverse("names");
		assertEquals("seman", result);
	}

}

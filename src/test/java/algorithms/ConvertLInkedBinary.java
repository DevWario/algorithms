package algorithms;

import algorithms.structures.ListNode;

public class ConvertLInkedBinary {
    public int getDecimalValue(ListNode head) {
    	int result = 0;
    	while(head != null) {
    		result = result * 2 + head.val;
    		head = head.next;
    	}
    	
    	return result;
    }
    
    public int getDecimalValue_(ListNode head) {
    	
    	//Find the size of array
    	int size = 0;
    	ListNode node = head;
    	
    	while(node != null) {
     		size++;
    		node = node.next;
    	}
    	
    	int[] integers = new int[size];
    	
    	node = head;
    	int index = 0;
    	while(node != null) {
    		integers[index] = node.val;
    		index++;
    		node = node.next;
    	}
    	
    	//reverse the array
    	
    	int start = 0;
    	int end = integers.length - 1;
    	
    	while(start > end) {
    		int temp = integers[start];
    		integers[start] = integers[end];
    		integers[end] = temp;
    		
    		start++;
    		end--;
    	}
    	
    	int base10Calculator = 1;
    	int result = 0;
    	
    	for (int i = 0; i < integers.length; i++) {
			result += integers[i] * base10Calculator;
			base10Calculator *= 2;
		}
    	
        return result;
    }
    

}

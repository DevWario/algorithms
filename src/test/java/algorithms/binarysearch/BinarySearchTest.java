package algorithms.binarysearch;

import static org.junit.jupiter.api.Assertions.assertEquals;

import algorithms.BinarySearch;
import org.junit.jupiter.api.Test;

public class BinarySearchTest {

	@Test
	public void testarMenor() {
		BinarySearch search = new BinarySearch(new int[] {1,3, 5, 7, 9});
		int index = search.search(3);
		assertEquals(index, 1);
	}

	@Test
	public void testarMaior() {
		BinarySearch search = new BinarySearch(new int[] {1,3, 5, 7, 9});
		int index = search.search(9);
		assertEquals(index, 4);
	}


	@Test
	public void testarIgual() {
		BinarySearch search = new BinarySearch(new int[] {1,3, 5, 7, 9});
		int index = search.search(5);
		assertEquals(index, 2);
	}

	@Test
	public void testarNaoTem() {
		BinarySearch search = new BinarySearch(new int[] {1,3, 5, 7, 9});
		int index = search.search(8);
		assertEquals(index, -1);
	}

}

package algorithms.binarysearch;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import algorithms.BinarySearchRotated;

public class BinarySearchRotatedTest {
	
	BinarySearchRotated binarySearch;
	
	@BeforeEach
	public void setUp() {
		binarySearch = new BinarySearchRotated();
	}

	@Test
	public void testSimpleArray() {
		int[] array = {1,3,5,7,9,13,15};
		int searchValue = 13;
		int actual = binarySearch.search(array, searchValue);
		assertEquals(5, actual);
	}

	@Test
	public void testRotatedArray() {
		int[] array = {9,13,15,1,3,5,7};
		int searchValue = 13;
		int actual = binarySearch.search(array, searchValue);
		assertEquals(1, actual);
	}
}

package algorithms;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SequenceRecursive {
	
	private boolean isSequence(int[] array, int index) {
		if(index < 1) {
			return true;
		}
		return array[index] - array[index - 1] == 1 && isSequence(array, index -1);
	}
	
	@Test
	public void testRecursiveTrue(){
		int[] array = {3, 4, 5, 6, 7, 8, 9};
		boolean actual = isSequence(array, array.length - 1);
		assertTrue(actual);
	}

	
	@Test
	public void testRecursiveFalse(){
		int[] array = {3, 4, 5, 6, 8, 9};
		boolean actual = isSequence(array, array.length - 1);
		assertFalse(actual);
	}

}

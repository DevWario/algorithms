package algorithms;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TestReverse {
	
	@Test
	public void testReverseArrayEven() {
		int[] array = new int[]{1, 5 , 3 , 7 , 9 , 12};
		ReverseElements revElements = new ReverseElements();
		int[] reverseArray = revElements.reverse(array);
		assertArrayEquals(reverseArray, new int[] {12, 9, 7, 3, 5, 1});
		
	}

	@Test
	public void testReverseArrayOdd() {
		int[] array = new int[]{1, 5 , 3 , 7 , 9 };
		ReverseElements revElements = new ReverseElements();
		int[] reverseArray = revElements.reverse(array);
		assertArrayEquals(reverseArray, new int[] {9, 7, 3, 5, 1});
		
	}


}

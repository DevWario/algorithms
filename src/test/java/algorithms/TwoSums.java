package algorithms;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TwoSums {
	
	public int[] sum(int[] array, int target) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < array.length; i++) {
			Integer value = map.get(target - array[i]);
			if(value == null) {
				map.put(array[i], i);
			}
			else {
				return new int[] {value, i};
			}
		}
		return null;
		
	}
	
	@Test
	public void testName() throws Exception {
		int[] expected = {0, 1};
		int[] array = {2, 7, 11, 15};
		int[] actual = sum(array, 9);
		
		assertArrayEquals(expected, actual);
		
	}

}

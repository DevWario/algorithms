package algorithms;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class JewelsAndStones {

    public int numJewelsInStones(String J, String S) {
    	int count = 0;
    	Map<String, Integer> map = new HashMap<String, Integer>();
    	char[] jewels = J.toCharArray();
    	for (int i = 0; i < jewels.length; i++) {
			map.put(Character.toString(jewels[i]),1);
		}
    	
    	char[] stones = S.toCharArray();
    	for (int i = 0; i < stones.length; i++) {
    		Integer value = map.get(Character.toString(stones[i]));
    		if(value != null) {
    			count++;
    		}
		}
    	
        return count;
    }
    
    @Test
	public void testName() throws Exception {
		int result = numJewelsInStones("aA", "aAAbbbb");
	}
    
    @Test
	public void testName1() throws Exception {
		System.out.println(8 % 5);
	}
}

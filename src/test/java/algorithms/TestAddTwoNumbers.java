package algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

import algorithms.structures.ListNode;
import org.junit.jupiter.api.Test;

public class TestAddTwoNumbers {
	
	@Test
	@DisplayName("First Implementation")
	public void testAddTwoNumbers() {
		ListNode list1 = new ListNode(2);
		ListNode list1_1 = new ListNode(4);
		ListNode list1_2 = new ListNode(3);
		list1.next = list1_1;
		list1_1.next = list1_2;

		ListNode list2 = new ListNode(5);
		ListNode list2_1 = new ListNode(6);
		ListNode list2_2 = new ListNode(4);
		list2.next = list2_1;
		list2_1.next = list2_2;
		
		AddTwoNumbersConcise add = new AddTwoNumbersConcise();
		ListNode node = add.addTwoNumbers(list1, list2);
		Assertions.assertEquals(node, node);

	}

}

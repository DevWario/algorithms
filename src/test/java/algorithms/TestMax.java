package algorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestMax {
	
	@Test
	public void testCopyArray() {
		int[] array = new int[]{1, 5 , 3 , 7 , 9 };
		Max max = new Max();
		int maxNumber = max.findMax(array);
		assertEquals(maxNumber, 9);
		
	}

	@Test
	@DisplayName("Test the algorithm with negative numbers")
	public void testMaxNumbersWithNegativeNumbers() {
		int[] array = new int[]{1, -5 , 3 , 7 , -9 };
		Max max = new Max();
		int maxNumber = max.findMax(array);
		assertEquals(maxNumber, 7);
		
	}

}

package algorithms;

import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class MinimumWordsInSentence {

    @Test
    public void givenStringThanReturnMinimumWords(){
        //solution([0, 4, -1, 0, 3], [0, -2, 5, 0, 3]);
    }

    public int solution(int[] A, int[] B) {
        // write your code in Java SE 8
        int k = A.length;

        int sumA = 0;
        int sumB = 0;
        int result = 0;

        for(int i = 0; i < k; i++){
            sumA += A[i];
            sumB += B[i];
        }


        if(sumA != sumB){
            return 0;
        }

        sumA = 0;
        sumB = 0;

        int half = sumA / 2;

        for(int i = 0; i < k - 1; i++){
            sumA += A[i];
            sumB += B[i];

            if(sumA == sumB && sumA == half){
                result++;
            }
        }


        return result;
    }

}

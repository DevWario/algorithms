package algorithms;

import algorithms.structures.ListNode;

public class AddTwoNumbersALgorithm {

	
	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        StringBuilder sb1 = new StringBuilder(Integer.toString(l1.val));
        while (l1.next != null){
            l1 = l1.next;
            sb1.append(l1.val);
        }
        
        StringBuilder sb2 = new StringBuilder(Integer.toString(l2.val));
        while (l2.next != null){
            l2 = l2.next;
            sb2.append(l2.val);
        }
        long number1 = Long.parseLong(sb1.reverse().toString());
        long number2 = Long.parseLong(sb2.reverse().toString());
        
        long sum = number1 + number2;
        
        String stringSum = new StringBuilder(Long.toString(sum)).reverse().toString();
        char[] charString = stringSum.toCharArray();
        
        ListNode result = new ListNode(Integer.parseInt(Character.toString(charString[0])));
        ListNode node = result;
        
        for(int i = 1; i < charString.length; i++){
            node.next = new ListNode(Integer.parseInt(Character.toString(charString[i])));
            node = node.next;
        }
        
        return result;
    }

}

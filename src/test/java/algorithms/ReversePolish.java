package algorithms;

import java.util.Stack;

public class ReversePolish {
	
    public int evalRPN(String[] tokens) {
        Stack<String> stack = new Stack<>();
        for(int i = 0; i < tokens.length; i++){
            if(isNumber(tokens[i])){
                stack.add(tokens[i]);
            } else {
                int a = Integer.parseInt(stack.pop());
                int b = Integer.parseInt(stack.pop());
                
                switch(tokens[i]){
                    case "+": stack.add((a + b) + "");
                    case "-": stack.add((a - b) + "");
                    case "/": stack.add((a / b) + "");
                    case "*": stack.add((a * b) + "");
                }
            }
        }
        
        return Integer.parseInt(stack.pop());
    }
    
    private boolean isNumber(String pNumber){
        try{
            Long.parseLong(pNumber);
            return true;
        }catch(Exception e){
            return false;
        }
    }


}

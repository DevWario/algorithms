package algorithms;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class PrintTreeTest {
	
	private static final int FROM_ZERO = 0;

	@Test
	public void printTreeFromLeftTest(){
		printTreeFromLeft(10);
	}

	@Test
	public void printTreeFromRightTest(){
		printTreeFromRight(10);
	}

	@Test
	public void printTreeFromCenterTest(){
		printTreeFromCenter(11);
	}

	private void printTreeFromRight(int size) {
		printHeader("Print tree from right");
		var charArray = getInitialCharArray(size);
		for (int from = size - 1; from >= 0; from--) {
			Arrays.fill(charArray, from, size, '*');
			System.out.println(String.valueOf(charArray));
		}
	}

	private void printHeader(String caption) {
		System.out.println("====="+caption+"=====");
	}

	private void printTreeFromLeft(int size) {
		printHeader("Print tree from left");
		var charArray = getInitialCharArray(size);
		for (int to = 1; to <= size; to++) {
			Arrays.fill(charArray, FROM_ZERO, to, '*');
			System.out.println(String.valueOf(charArray));
		}
	}

	private void printTreeFromCenter(int size) {
		printHeader("Print tree from center");
		var charArray = getInitialCharArray(size);
		int left = size / 2;
		int right = left + 1;
		while(left >= 0 && right <= size) {
			Arrays.fill(charArray, left, right, '*');
			System.out.println(String.valueOf(charArray));
			left--;
			right++;
		}
	}

	private char[] getInitialCharArray(int size) {
		var charArray = new char[size];
		Arrays.fill(charArray, '_');
		return charArray;
	}

}

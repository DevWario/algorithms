package algorithms;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class DecompressedRLE {
	
    public int[] decompressRLElist(int[] nums) {

    	int totalFrequency = 0;
    	for (int i = 0; i < nums.length; i+=2) {
    		totalFrequency += nums[i];
		}        
    	
    	int[] decompressArray = new int[totalFrequency];
    	int decompressIndex = 0;
    	for (int i = 0; i < nums.length; i+=2) {
    		for (int j = 0; j < nums[i]; j++) {
				decompressArray[decompressIndex] = nums[i + 1];
				decompressIndex++;
			}
		}   
    	
    	return decompressArray;

    }
    
    @Test
	public void testName() throws Exception {
		int[]  nums = {1,2,3,4};
		int[] expected = {2,4,4,4};
		int[] result = decompressRLElist(nums);
		
		assertArrayEquals(expected, result);
	}


}

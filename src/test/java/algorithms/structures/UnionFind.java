package algorithms.structures;

import java.util.Arrays;

public class UnionFind {
	
	private int[] array;
	
	public UnionFind(int size) {
		array = new int[size];
		for (int i = 0; i < array.length; i++) {
			array[i] = i;
		}
	}
	
	public void union(int p, int q) {
    	System.out.println("Older array arrange: "+Arrays.toString(array));
		int pvalue = array[p];
		int qvalue = array[q];
		for (int i = 0; i < array.length; i++) {
			if(array[i] == pvalue) {
				array[i] = qvalue;
			}
		}
    	System.out.println("New array arrange: "+Arrays.toString(array));
		
	}
	
	public boolean connected(int p, int q) {
		return array[p] == array[q];
	}

}

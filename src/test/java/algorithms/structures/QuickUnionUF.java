package algorithms.structures;

public class QuickUnionUF {
	
	private int[] array;
	private int[] sizeTree;
	
	public QuickUnionUF(int size) {
		array = new int[size] ;
		sizeTree = new int[size] ;
		for (int i = 0; i < array.length; i++) {
			array[i] = i;
			sizeTree[i] = 1;
		}
	}
	
	public boolean connected(int element1, int element2) {
		return root(element1) == root(element2);
	}
	
	public void union(int element1, int element2) {
		int rootElement1 = root(element1); 
	    int rootElement2 = root(element2);
	    
	    if(rootElement1 == rootElement2) {
	    	return;
	    }
	    
	    if(sizeTree[rootElement1] > sizeTree[rootElement2]) {
			array[rootElement2] = rootElement1;
			sizeTree[rootElement2] += element1;
	    } else {
			array[rootElement1] = rootElement2;
			sizeTree[rootElement1] += element2;
	    }
	}
	
	private int root(int element) {
		while(array[element] != element) {
			array[element] = array[array[element]];
			element = array[element];
		}
		return element;
	}

}

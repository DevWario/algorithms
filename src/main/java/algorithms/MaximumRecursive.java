package algorithms;

public class MaximumRecursive {
	
	public int maximum(int[] array , int index) {
		if(index == 0) {
			return array[0];
		}
		return Math.max(array[index], maximum(array, index -1));
	}

}

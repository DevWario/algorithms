package algorithms;

public class MySelectionSort {

	public int[] sort(int[] array) {
		int length = array.length;
		
		while (length > 0) {
			
			int maxIndex = 0;
 
			for (int i = 0; i < length; i++) {
				
				if(array[maxIndex] < array[i]) {
					maxIndex = i;
				}

			}
			
			int temp  = array[maxIndex];
			array[maxIndex] = array[length - 1];
			array[length - 1] = temp;
			
			length--;
			
		}
		
		return array;
	}
	

}

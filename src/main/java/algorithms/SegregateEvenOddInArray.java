package algorithms;

public class SegregateEvenOddInArray {

	public static void segregate(int[] array) {
		int left = 0 , right = array.length -1;
		
		while(left < right) {
			
			while(array[left] % 2 == 0 && left < right) {
				left++;
			}
			
			while(array[right] % 2 == 1 && left < right) {
				right--;
			}
			
			if(left < right) {
				int temp = array[left];
				array[left] = array[right];
				array[right] = temp;
				left++;
				right--;
			}
			
		}
	}

}

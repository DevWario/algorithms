package algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class DynamicArray {
	
	private static final int Y = 2;
	private static final int X = 1;
	private static final int QUERY_COMMAND = 0;
	static Integer lastAnswer = 0;

/*
     * Complete the 'dynamicArray' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. INTEGER n
     *  2. 2D_INTEGER_ARRAY queries
     */

    public static List<Integer> dynamicArray(int n, List<List<Integer>> queries) {
    	
    	List<List<Integer>> seqList = new ArrayList<>();
    	IntStream.range(0, n).forEach((action) -> seqList.add(new ArrayList<>()));
    	List<Integer> result = new ArrayList<>();
    	
    	
    	queries.forEach(query -> {
        
        	int queryCommand = query.get(QUERY_COMMAND);
    		int x = query.get(X);
    		int y = query.get(Y);
    		
			List<Integer> seq = seqList.get((x ^ lastAnswer) % n);

			if(queryCommand == X) {
				seq.add(y);
    		} else {
    			lastAnswer = seq.get(y % seq.size());
    			result.add(lastAnswer);
    		}
    		
    	});
    	
    	return result;

    }

}

package algorithms;

public class SecondLargeElement {
	
	public int find(int[] array) {
		
		int max = Integer.MIN_VALUE + 1;		
		int secondMax = Integer.MIN_VALUE;		

		for (int i = 0; i < array.length; i++) {
			
			if(array[i] > max) {
				secondMax = max;
				max = array[i];
			} else if (array[i] > secondMax) {
				secondMax = array[i];
			}
			
		}
		
		return secondMax;
	}

}

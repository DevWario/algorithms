package algorithms;

public class BinarySearch {
	
	private int[] sortedArray;
	
	public BinarySearch(int[] sortedArray) {
		this.sortedArray = sortedArray;
	}

	public int search(int value) {
		
		int indexBegin = 0;
		int indexEnd = sortedArray.length -1;
		
		while(indexBegin <= indexEnd) {
			
			int average = (indexBegin + indexEnd) /2;
			
			if(value == sortedArray[average]) {
				return average;
			} else if(value < sortedArray[average]) {
				indexEnd = average - 1;
			} else {
				indexBegin = average + 1;
			}
		}
		
		return -1;
		
	}
	
	
	
}

package algorithms;

public class FindMinimumInRotatedArray {

	public int findMinimum(int[] rotatedArray) {
		int initialIndex = 0;
		int finalIndex = rotatedArray.length - 1;
		
		if(rotatedArray[initialIndex] < rotatedArray[finalIndex]) {
			return rotatedArray[initialIndex];
		}
		
		return findValue(rotatedArray, initialIndex, finalIndex);
	}

	private int findValue(int[] rotatedArray, int initialIndex, int finalIndex) {
		    int midIndex = initialIndex + (finalIndex - initialIndex) / 2;
		  
		    if(rotatedArray[midIndex - 1] > rotatedArray[midIndex]) {
		    	return rotatedArray[midIndex];
		    }
		
			if(rotatedArray[midIndex] > rotatedArray[finalIndex]) {
				initialIndex = midIndex;
			} else {
				finalIndex = midIndex;
			}
			
			return findValue(rotatedArray, initialIndex, finalIndex);
	}

}

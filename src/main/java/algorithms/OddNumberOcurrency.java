package algorithms;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class OddNumberOcurrency {
	
	
	public int find(int[] array) {
		
		Map<Integer, Integer> map = new HashMap<>();

		for (int i = 0; i < array.length; i++) {
			
			map.put(array[i], (map.get(array[i]) == null ? 0 : map.get(array[i]))+ 1);
			
		}
		
		for (Entry<Integer, Integer> entry: map.entrySet()) {
			if(entry.getValue() % 2 == 1) {
				return entry.getKey();
			}
		}
		
		
		
		
		return -1;
	}

}

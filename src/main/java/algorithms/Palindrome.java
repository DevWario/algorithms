package algorithms;

import java.util.Arrays;

public class Palindrome {
	
	public boolean isPalindrome(String word) {
		String palindrome = Arrays
				.stream(word.split(""))
				.reduce("",(parcialString, next) -> next + parcialString);
		return word.equals(palindrome);
	}

}

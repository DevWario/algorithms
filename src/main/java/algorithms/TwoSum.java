package algorithms;

import java.util.Arrays;
import java.util.Comparator;

public class TwoSum {
	
	
    private static final int INDEX = 1;
	private static final int VALUE = 0;

	//This approach dosent accept equal numbers.
    public int[] twoSumWithoutEquals(int[] nums, int target) {
    	
    	int[] arrayCopy = Arrays.copyOf(nums, nums.length);
    	
    	Arrays.sort(nums);
        
        int i = 0;
        int j = nums.length -1;
        
        while(i < j){
            
            int sum = nums[i] + nums[j];
            
            if(sum == target){
                return new int[]{
                		Arrays.binarySearch(arrayCopy, nums[i]),
                		Arrays.binarySearch(arrayCopy, nums[j])
                		};
            } else if(sum < target){
                ++i;
            } else {
                --j;
            } 
            
        }
        
        return null;
    
    }
	
    public int[] sum(int[] nums, int target) {
    	
    	int[][] arrayIndexed = new int[nums.length][2];

    	//We have a indexed array
    	for (int i = 0; i < nums.length; i++) {
			arrayIndexed[i][VALUE] = nums[i];
			arrayIndexed[i][INDEX] = i;
		}
    	
    	//Sort using values to compare, not the indexes.
    	Arrays.sort(arrayIndexed, new Comparator<int[]>() {

			@Override
			public int compare(int[] o1, int[] o2) {
				return Double.compare(o1[VALUE], o2[VALUE]);
			}
			
    		
		});
        
        int i = 0;
        int j = nums.length -1;
        
        while(i < j){
            
            int sum = arrayIndexed[i][VALUE] + arrayIndexed[j][VALUE];
            
            if(sum == target){
                return new int[]{
                		arrayIndexed[i][INDEX],
                		arrayIndexed[j][INDEX]
                		};
            } else if(sum < target){
                ++i;
            } else {
                --j;
            } 
            
        }
        
        return null;
    
    }
	    
}

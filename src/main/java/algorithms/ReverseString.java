package algorithms;

import java.util.Arrays;

public class ReverseString {
	
	public String reverse(String string) {
		return Arrays
				.stream(string.split(""))
				.reduce("", (parcialString, next) -> 
										next + parcialString);
	}

}

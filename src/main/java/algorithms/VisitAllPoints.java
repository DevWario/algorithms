package algorithms;

public class VisitAllPoints {
	
    public int minTimeToVisitAllPoints(int[][] points) {
        int seconds = 0;

        int x = points[0][0];            
        int y = points[0][1]; 
        
        for(int i = 1; i < points.length; i++){
            
            int x_dest = points[i][0];            
            int y_dest = points[i][1];
            
            while(x != x_dest && y != y_dest) {
                if(x_dest > x && y_dest > y ) {
                	x++;
                	y++;
                	seconds++;
                } else if(x_dest < x && y_dest < y) {
                	x--;
                	y--;
                	seconds++;
                } else if (x_dest > x ) {
                	x++;
                	seconds++;
                } else if(x_dest < x ) {
                	x--;
                	seconds++;
                } else if (y_dest > y ) {
                	y++;
                	seconds++;
                } else if(y_dest < y ) {
                	y--;
                	seconds++;
                } 
            }
            
        }
        
        return seconds;
    }

}

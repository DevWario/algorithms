package algorithms;

import java.util.Arrays;

public class RotateArray {

	public int[] rotateMethod1(int[] array, int target) {
		int sizeArray = array.length;
		int[] result = new int[sizeArray];
		
		if(target > sizeArray) {
			target = target % sizeArray;
		}
		
		for (int i = 0; i < target; i++) {
			result[i] = array[(sizeArray - target) + i];
		}
		
		for (int i = target; i < sizeArray; i++) {
			result[i] = array[i - target];
		}
		
		return result;
	}
	
	public int[] rotateMethod2(int[] array, int target) {
		
		for (int i = 0; i < target; i++) {
			for (int j = array.length - 1; j > 0; j--) {
				int temp = array[j];
				array[j] = array[j - 1];
				array[j - 1] = temp;
				System.out.println("Array temporario::: "+Arrays.toString(array));
			}
		}
		
		return array;
	}
	
	public static void main(String[] args) {
		RotateArray array = new RotateArray();
		int[] entrada = new int[] {1, 2 ,3 ,4, 5, 6};
		System.out.println("Entrada: "+Arrays.toString(entrada));
		int[] newArray = array.rotateMethod2(entrada,2);
		System.out.println("Sa�da: "+Arrays.toString(newArray));
	}
	
}

package algorithms;

public class ArrayCopy {
	
	public int[] copy(int[] array) {
		
		var arrayCopy = new int[array.length];
		
		for (int i = 0; i < array.length; i++) {
			arrayCopy[i] = array[i];
		}
		
		return arrayCopy;
		
	}

}

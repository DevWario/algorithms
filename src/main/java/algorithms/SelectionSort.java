package algorithms;

public class SelectionSort {
	
	public int[] sort(int[] array) {
		
		for (int i = 0; i < array.length - 1; i++) {
			int indexMin = i;
			for(int j = i + 1; j < array.length; j++) {
				if(array[j] < array[indexMin]) {
					indexMin = j;
				}
			}
			swap(array, indexMin, i);
		}
		return array;
	}

	private void swap(int[] array, int indexMin, int i) {
		int temp = array[indexMin];
		array[indexMin] = array[i];
		array[i] = temp;
	}
	
}

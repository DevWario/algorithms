package algorithms;

import java.util.Iterator;
import java.util.TreeSet;

public class Solution {

	public int solution(int[] A) {
		
		
		
		TreeSet<Integer> set = new TreeSet<>();
		for (int i = 0; i < A.length; i++) {
			if(A[i] > 0) {
				set.add(A[i]);
			}
		}
		
		if (set.size() == 0) {
			return 1;
		}
		
		if(set.first() > 1) {
			return 1;
		}


		
		if (set.size() == 1) {
			if (set.last() == 1) {
				return 2;
			} else {
				return 1;
			}

		} else {

			Iterator<Integer> iterator = set.iterator();
			Integer anterior = null;
			while (iterator.hasNext()) {
				Integer atual = iterator.next();
				if (anterior != null) {
					int diferenca = atual - anterior;
					if (diferenca > 1) {
						return anterior + 1;
					}
				}
				anterior = atual;
			}

		}
		return set.last() + 1;
	}

}

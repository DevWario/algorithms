package algorithms.sort;

public interface Sortable {
	
	void sort(int[] array);

}

package algorithms;

import java.util.Arrays;

public class InsertionSort {
	
	public InsertionSort() {
	}
	
	public int[] _sort(int[] array) {
		
		for (int initialUnsortedIndex = 1; 
			 initialUnsortedIndex < array.length; 
			 initialUnsortedIndex++) {
			
			int unsortedIndex = initialUnsortedIndex;
			int lastSortedIndex = initialUnsortedIndex -1;
			while(lastSortedIndex >= 0 
					&& array[lastSortedIndex] > array[unsortedIndex]) {
				
				int temp = array[lastSortedIndex];
				array[lastSortedIndex] = array[unsortedIndex];
				array[unsortedIndex] = temp;
				unsortedIndex = lastSortedIndex; 
				
				--lastSortedIndex;
			}
		}
		
		return array;
	}
	
	public int[] sortCorrect(int[] array) {
		System.out.println("Ärray embaralhaado: "+ Arrays.toString(array));
		
		for (int unsortedIndex = 1; 
			 unsortedIndex < array.length; 
			 unsortedIndex++) {
			
			
			int valueToSort = array[unsortedIndex];
			
			int lastSortedIndex = unsortedIndex;
 
			 while(lastSortedIndex > 0 
					&& array[lastSortedIndex -1] > valueToSort) {
				
				array[lastSortedIndex] = array[lastSortedIndex -1];
				System.out.println("Ärray parcial: "+ Arrays.toString(array));
				
				--lastSortedIndex;
			}
			
			array[lastSortedIndex] = valueToSort;
		}
		System.out.println("Ärray final: "+ Arrays.toString(array));

		return array;
	}
	
	public int[] sort(int[] array) {
		
		for(int i= 1; i < array.length; i++) {
			int newElement = array[i];
			int j;
			for (j = i; j > 0 && array[j -1] > newElement; j--) {
				array[j] = array[j - 1];
			}
			array[j] = newElement;
		}
		
		return array;
	}
	
}

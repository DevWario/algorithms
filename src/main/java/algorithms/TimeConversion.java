package algorithms;

import java.io.IOException;
import java.util.Scanner;

public class TimeConversion {

	/*
	 * Complete the timeConversion function below.
	 */
	static String timeConversion(String s) {
		/*
		 * Write your code here.
		 */
		String ampm = s.substring(8, 10);
		String hour = s.substring(0, 2);
		String minSec = s.substring(2, 8);
		String newHour = "";

		if (ampm.equals("AM")) {
			newHour = defineAmHour(hour);
		} else {
			newHour = definePmHour(hour);
		}

		return newHour + minSec;

	}
	
	private static String defineAmHour(String hour) {
		String newHour;
		if ("12".equals(hour)) {
			newHour = "00";
		} else {
			newHour = hour;
		}
		return newHour;
	}

	private static String definePmHour(String hour) {
		if ("12".equals(hour)) {
			return hour = "12";
		}

		int numericHour = Integer.parseInt(hour);
		int newNumericHour = numericHour + 12;
		return String.format("%02d", newNumericHour);
	}

	private static final Scanner scan = new Scanner(System.in);

	public static void main(String[] args) throws IOException {

		String s = scan.nextLine();

		String result = timeConversion(s);

		System.out.println(result);

		scan.close();

	}
}

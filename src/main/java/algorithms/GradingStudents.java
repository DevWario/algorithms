package algorithms;

import java.io.IOException;
import java.util.Scanner;

public class GradingStudents {

	    /*
	     * Complete the gradingStudents function below.
	     */
	    static int[] gradingStudents(int[] grades) {
	    	
	    	for (int i = 0; i < grades.length; i++) {
	    		if(grades[i] >= 38) {
	    			grades[i] = roundGrade(grades[i]);
	    		}
			}
	    	
	    	return grades; 

	    }
	    

	    private static int roundGrade(int grade) {
	    	int multiple = getNextMultiple5(grade);
	    	if(multiple - grade < 3) {
	    		grade = multiple;
	    	}
			return grade;
		}


		private static int getNextMultiple5(int grade) {
			return grade + (5 - (grade % 5));
		}


		private static final Scanner scan = new Scanner(System.in);

	    public static void main(String[] args) throws IOException {

	        int n = Integer.parseInt(scan.nextLine().trim());

	        int[] grades = new int[n];

	        for (int gradesItr = 0; gradesItr < n; gradesItr++) {
	            int gradesItem = Integer.parseInt(scan.nextLine().trim());
	            grades[gradesItr] = gradesItem;
	        }

	        int[] result = gradingStudents(grades);

	        for (int resultItr = 0; resultItr < result.length; resultItr++) {
	        	System.out.println(result[resultItr]);

	            if (resultItr != result.length - 1) {
	            	System.out.println("\n");
	            }
	        }

	    }
	}

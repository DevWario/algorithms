package algorithms;

import java.io.IOException;
import java.util.Scanner;

public class CountingValleys {

    // Complete the countingValleys function below.
    static int countingValleys(int n, String s) {
    	int seaLevel = 0;
    	int countValleys = 0;
    	boolean startValley = false;
    	char[] path = s.toCharArray();
    	for (int i = 0; i < path.length; i++) {
			if(path[i] =='D'){
				if(seaLevel == 0) {
					startValley = true;
				}
				seaLevel--;
			} else if (path[i] == 'U'){
				if(startValley && seaLevel == -1) {
					countValleys+= 1;
					startValley = false;
				}
				seaLevel++;
			}
		}
    	//System.out.printf("Numero de passos: %d caminhada: %s", n, s);
    	return countValleys;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String s = scanner.nextLine();

        int result = countingValleys(n, s);

        System.out.println(result);

        scanner.close();
    }
}

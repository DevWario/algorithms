package algorithms;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class PileOfSocks {

	   // Complete the sockMerchant function below.
    static int sockMerchant(int n, int[] ar) {
    	Map<Integer, Integer> hashMap = new HashMap<Integer,Integer>();
    	for (int i = 0; i < n; i++) {
			hashMap.put(ar[i], (hashMap.get(ar[i]) == null ? 0 : hashMap.get(ar[i])) +1);
			System.out.println(hashMap);
		}
    	
    	int totalPairsOfSocks = 0;
    	
    	for(Integer value: hashMap.values()) {
    		int divisor = value/2;
    		if(divisor >= 1) {
    			totalPairsOfSocks += divisor;
    		}
    	}
    	
    	return totalPairsOfSocks;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] ar = new int[n];

        String[] arItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arItem = Integer.parseInt(arItems[i]);
            ar[i] = arItem;
        }

        int result = sockMerchant(n, ar);

        System.out.println(result);
        scanner.close();
    }
}

package algorithms;

public class MaxConsecutiveOnes {
	
	private static final int ONE = 1;

	public int getMaxConsecutiveOnes(int[] array) {
		
		int maxOnes = 0, sum = 0;
		
		for (int i = 0; i < array.length; i++) {

			if(array[i] == ONE) {
				sum++;
			} else {
				sum = 0;
			}
			
			if(sum > maxOnes) {
				maxOnes = sum;
			}
			
		}
		
		return maxOnes;
	}

	public int getMaxConsecutiveOnesCustomized(int[] array) {
		
		int maxOnes = 0;
		int sum = 0;
		int sumInverse = 0;
		
		int indexBegin = 0; 
		int indexEnd = array.length - 1;
		
		if(array.length == ONE) {
			if(array[0] == ONE) {
				return ONE;
			} else {
				return 0;
			}
		}
		
		while (indexBegin < indexEnd) {
			
			if(array[indexBegin] == ONE) {
				sum++;
			} 
			
			if(array[indexEnd] == ONE) {
				sumInverse++;
			} 
			
			if(indexEnd - indexBegin == ONE) {
				if(array[indexBegin] == ONE &&
				   array[indexEnd] == ONE &&
						sum > 0 && sumInverse > 0 ) {
					sum+=sumInverse;
				}
			}
			
			if(sum > maxOnes) {
				maxOnes = sum;
			}
			
			if(sumInverse > maxOnes) {
				maxOnes = sumInverse;
			}
			
			if(array[indexBegin] == 0) {
				sum = 0;
			} 
			
			if(array[indexEnd] == 0) {
				sumInverse = 0;
			} 

			indexBegin++;
			indexEnd--;
			
		}
		
		return maxOnes;
	}
	
	public int getMaxConsecutiveOnesOptimized(int[] array) {
		
		int max = 0;
		int currentIndex = 0;

		while ( currentIndex < array.length) {
			
			if(array[currentIndex] == 1) {

				int currentLength = 1;
				currentIndex++;

				while(currentIndex < array.length && array[currentIndex] == 1) {
					currentLength++;
					currentIndex++;
				}

				max = Math.max(max, currentLength);
				
				currentIndex++;
				
			}
		}
		
		return max;
	}

}

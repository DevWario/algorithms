package algorithms;

public class BinarySearchRotated {

	public int search(int[] array, int searchValue) {
		int indexLow = 0;
		int indexHigh = array.length - 1;
		
		while(indexLow <= indexHigh) {

			int middle = indexLow + ((indexHigh - indexLow)/ 2);
			
			if(array[middle] == searchValue) {
				return middle;
			}
			
			if(array[middle] <= array[indexHigh]) {
				
				if(searchValue > array[middle] && searchValue <= array[indexHigh]) {
					indexLow = middle + 1;
				} else {
					indexHigh = middle -1;
				}
				
			} else {
				
				if(searchValue > array[indexLow] && searchValue < array[middle]) {
					indexHigh = middle - 1;
				} else {
					indexLow = middle + 1;
				}
				
			}
			
		}
		
		return -1;
	}

}

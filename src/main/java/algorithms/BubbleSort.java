package algorithms;

public class BubbleSort {
	
	public int[] sort(int[] array) {
		int length = array.length;
		
		
		while (length > 0) {
			
			for (int i = 0; i < array.length -1; i++) {

				if(array[i] > array[i + 1]) {
					int temp  = array[i];
					array[i] = array[i + 1];
					array[i + 1] = temp;
				}
				
			}
			
			length--;
			
		}
		
		return array;
	}
	
	

}

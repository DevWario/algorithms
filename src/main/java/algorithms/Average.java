package algorithms;

public class Average {
	
	public int calculateAvarage(int[] array) {
		
		int sum = 0;
		
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		
		return sum / array.length; 
		
	}

}

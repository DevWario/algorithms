package algorithms;

public class BinarySearchRecursion {
	
	private int[] sortedArray;

	public BinarySearchRecursion(int[] sortedArray) {
		this.sortedArray = sortedArray;
		
	}
	
	public int search(int value) {
		return binarySearch(0, sortedArray.length -1, value);
	}
	
	public int binarySearch(int indexBegin, int indexEnd, int value) {

		if(indexBegin > indexEnd) {
			return -1;
		}

		int average = (indexBegin + indexEnd) / 2;
		
		if(sortedArray[average] == value) {
			return average;
		} else if(value < sortedArray[average]) {
			return binarySearch(indexBegin, average -1, value);
		} else {
			return binarySearch(average + 1, indexEnd, value);
		}

	}

}
